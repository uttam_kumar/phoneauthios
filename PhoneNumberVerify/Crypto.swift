//
//  Crypto.swift
//  PhoneNumberVerify
//
//  Created by Syncrhonous on 12/6/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//
import UIKit
import Foundation
import CommonCrypto
class Crypto {
    
    private var key: Data

    //constructor
    init(_ theDigest: Data) {
        self.key = theDigest
    }
    
    //encrypt part
    public func encrypt(_ clear: String) -> Data? {
        let input = clear.data(using: .utf8)!
        var cipherText = Data(count: input.count + kCCBlockSizeAES128)
        let keyLength = size_t(kCCKeySizeAES128)
        assert(key.count >= keyLength)
        let operation: CCOperation = UInt32(kCCEncrypt)
        let algoritm: CCAlgorithm = UInt32(kCCAlgorithmAES128)
        let options: CCOptions = UInt32(kCCOptionECBMode + kCCOptionPKCS7Padding)
        
        var ctLength: size_t = 0
        
        let cryptStatus = key.withUnsafeBytes {keyBytes in
            input.withUnsafeBytes {inputBytes in
                cipherText.withUnsafeMutableBytes {mutableBytes in
                    CCCrypt(operation, algoritm, options,
                            keyBytes.baseAddress, keyLength, nil,
                            inputBytes.baseAddress, inputBytes.count,
                            mutableBytes.baseAddress, mutableBytes.count,
                            &ctLength)
                }
            }
        }
        if cryptStatus == CCCryptorStatus(kCCSuccess) {
            cipherText.count = Int(ctLength)
            return cipherText
        } else {
            return nil
        }
    }
    
    //decrypt part
    public func decrypt(_ buf: Data, _ offset: Int, _ len: Int) -> Data? {
        var dec = Data(count: len)
        let keyLength = size_t(kCCKeySizeAES128)
        let operation: CCOperation = UInt32(kCCDecrypt)
        let algoritm: CCAlgorithm = UInt32(kCCAlgorithmAES128)
        let options: CCOptions = UInt32(kCCOptionECBMode + kCCOptionPKCS7Padding)
        
        var ctLength :size_t = 0
        
        let cryptStatus = key.withUnsafeBytes {keyBytes in
            buf.withUnsafeBytes {inputBytes in
                dec.withUnsafeMutableBytes {mutableBytes in
                    CCCrypt(operation, algoritm, options,
                            keyBytes.baseAddress, keyLength, nil,
                            inputBytes.baseAddress, inputBytes.count,
                            mutableBytes.baseAddress, mutableBytes.count,
                            &ctLength)
                }
            }
        }
        if cryptStatus == CCCryptorStatus(kCCSuccess) {
            dec.count = Int(ctLength)
            return dec
        } else {
            return nil
        }
    }
}
