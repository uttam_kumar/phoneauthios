//
//  VerificationCodeViewController.swift
//  PhoneNumberVerify
//
//  Created by Syncrhonous on 4/3/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import FirebaseAuth
import CommonCrypto

class VerificationCodeViewController: UIViewController {
    
    
    @IBOutlet weak var codeText: UITextField!
    
    @IBOutlet weak var statusVerifyText: UILabel!
    var crypto: Crypto? = nil
    
    var token = ""
    var vphone = ""
    var api_url = ""
    var rtmp_url = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("verification code view controller")
        // Do any additional setup after loading the view.
    }
    
    func MD5(_ data: Data) -> Data {
        let length = Int(CC_MD5_DIGEST_LENGTH)
        var digest = Data(count: length)
        
        data.withUnsafeBytes {bytes in
            _ = digest.withUnsafeMutableBytes {mutableBytes in
                CC_MD5(bytes.baseAddress, CC_LONG(bytes.count), mutableBytes.bindMemory(to: UInt8.self).baseAddress)
            }
        }
        
        return digest
    }

    //http request here
    func startHttpRquest(token: String){
        let strongToken = "YourStrongKey" + token
        print("strongToken: \(strongToken)")
        print("token length: \(token.count)")
        let tokenRes = self.MD5(strongToken.data(using: .utf8)!)
        //print("\n\ntoken: res : \(tokenRes)")
        self.crypto = Crypto(tokenRes)
        
        let msg = [
            "phone": vphone,
            "shortcode": "1001",
        ]
        let msgData = try! JSONSerialization.data(withJSONObject: msg)
    
        let ss = String(data: msgData, encoding: .utf8)!
        print(ss)
        let eneMsg = self.crypto!.encrypt(ss)!
        print(eneMsg)
        
        let reque = "http://seeds.synopi.com:8002/resolve/\(eneMsg.hexEncodedString())/\(token)"
        print("\n\nurl: \(reque)")
        
        DispatchQueue.main.async {
            self.simpleGetUrlRequest(url: reque)
        }
    }
    
    //first request
    func simpleGetUrlRequest(url: String){
        let myUrl = NSURL(string: url)
        // Creaste URL Request
        let request = NSMutableURLRequest(url: myUrl! as URL)
        request.httpMethod = "GET"
        var responseString = ""
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error!)")
                responseString = ""
            }else{
            // Print out response string
                responseString = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                print("responseString = \(responseString)")
                let dat: Data = self.hexStringToData(responseString)
                DispatchQueue.main.async {
                    let dec = (self.crypto?.decrypt(dat, 0, dat.count)  ?? nil)
                    if dec != nil {
                        do {
                            if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: dec!, options: []) as? NSDictionary {
                                // Print out dictionary
                                print(convertedJsonIntoDict)
                                // Get value by key
                                self.api_url = (convertedJsonIntoDict["api_url"] as? String)!
                                print("api_url: \(self.api_url)")
                                self.onApiUrl(api_url: self.api_url)
                            }
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    func simpleGetUrlRequestForStreamKey(url: String){
        let myUrl = NSURL(string: url)
        // Creaste URL Request
        let request = NSMutableURLRequest(url: myUrl! as URL)
        request.httpMethod = "GET"
        var responseString = ""
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            // Check for error
            if error != nil
            {
                print("error=\(error!)")
                responseString = ""
            }else{
                // Print out response string
                responseString = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                print("responseString = \(responseString)")
                let dat: Data = self.hexStringToData(responseString)
                DispatchQueue.main.async {
                    let dec = (self.crypto?.decrypt(dat, 0, dat.count)  ?? nil)
                    if dec != nil {
                        do {
                            if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: dec!, options: []) as? NSDictionary {
                                // Print out dictionary
                                print(convertedJsonIntoDict)
                                // Get value by key
                                self.rtmp_url = (convertedJsonIntoDict["rtmp_url"] as? String)!
                                print("rtmp_url: \(self.rtmp_url)")
                                //self.onApiUrl(api_url: self.rtmp_url)
                                let splashIndex = self.rtmp_url.lastIndex(of: "/")
                                let indexValue = self.rtmp_url.distance(from: self.rtmp_url.startIndex, to: splashIndex!) + 1
                                let s_url = self.rtmp_url.prefix(indexValue)
                                let s_key = self.rtmp_url.dropFirst(indexValue)
                                print(s_url)
                                print(s_key)
                                self.printUrlkey(url: String(s_url), key: String(s_key))
                            }
                        } catch let error as NSError {
                            print(error.localizedDescription)
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    //
    func onApiUrl(api_url: String){
        let msg = [
            "phone": vphone,
        ]
        let msgData = try! JSONSerialization.data(withJSONObject: msg)
        
        let ss = String(data: msgData, encoding: .utf8)!
        print(ss)
        let eneMsg = self.crypto!.encrypt(ss)!
        print(eneMsg)
        
        let reques = "\(api_url)/\(eneMsg.hexEncodedString())/\(token)"
        print("\n\nsecond url: \(reques)")
        
        DispatchQueue.main.async {
            self.simpleGetUrlRequestForStreamKey(url: reques)
        }
    }

    
    func hexStringToData(_ s: String) -> Data {
        let len = s.count
        var data = Data(count: len/2)
        var start = s.startIndex
        for i in 0..<len/2 {
            let end = s.index(start, offsetBy: 2)
            data[i] = UInt8(s[start..<end], radix: 16)!
            start = end
        }
        return data
    }
    
    
    
    func printUrlkey(url: String, key: String){
        print("urlll: \(url)")
        print("urlll: \(key)")
        
        if(url != "" && key != ""){
            print("url and key is not empty")
        }
    }
    
    @IBAction func code_verify(_ sender: UIButton) {
        print("verify btn clicked")
        
        if (!codeText.text!.isEmpty){

            let defaults = UserDefaults.standard
            let credential: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: defaults.string(forKey: "authVID")!, verificationCode: codeText.text!)
            vphone = defaults.string(forKey: "authPhoneNum")!
            print("vphone: \(vphone)")
            print("verifying")
            Auth.auth().signIn(with: credential) { (user, error) in
                
                if error != nil {
                    print("error to verify otp code")
                    self.statusVerifyText.text = "Error to verify OTP code"
                    print("error: \(String(describing: error?.localizedDescription))")
                }else{
                    self.statusVerifyText.text = "Varify successed"
                    print("verify successed")
                    //print("Phone number: \(String(describing: user?.phoneNumber))")
                    
                    DispatchQueue.main.async {
                        let currentUser = Auth.auth().currentUser
                        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                            if error != nil {
                                // Handle error
                                return;
                            }
                            // Send token to your backend via HTTPS
                            // ...
                            self.token = String(idToken!.prefix(152))
                            self.startHttpRquest(token: self.token)                        }
                    }
                    self.performSegue(withIdentifier: "logged", sender: Any?.self)
                }
            }
            
        }else{
            print("empty otp code")
            self.statusVerifyText.text = "Empty OTP code"
        }
    }
    

    
    
    @IBAction func resend_code(_ sender: UIButton) {
        self.statusVerifyText.text = "Resend OTP code"
        let defaults = UserDefaults.standard
        let phoneNum = defaults.string(forKey: "authPhoneNum")!
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNum, uiDelegate: nil) { (verificationID, error) in
            if error != nil {
                self.statusVerifyText.text = "Error to send code"
                print("Auth Error: \(String(describing: error?.localizedDescription))")
            }else{
                self.statusVerifyText.text = "Code sent"
                let defaults = UserDefaults.standard
                defaults.set(phoneNum, forKey: "authPhoneNum")
                defaults.set(verificationID, forKey: "authVID")
            }
        }
        
    }
    
}



extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}
