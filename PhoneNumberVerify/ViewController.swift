//
//  ViewController.swift
//  PhoneNumberVerify
//
//  Created by Syncrhonous on 4/3/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var statusText: UILabel!
    @IBOutlet weak var phoneNum: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("first activity")
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    @IBAction func code_send(_ sender: UIButton) {
        print("code send btn clicked")
        if(!phoneNum.text!.isEmpty){
            let phone = "+880" + phoneNum.text!
            print(phone)
            let alert = UIAlertController(title: "Phone number", message: "Is this your phone number? \n\(phone)", preferredStyle: .alert)
            let action = UIAlertAction(title: "Yes", style: .default){ (UIAlertAction) in
                PhoneAuthProvider.provider().verifyPhoneNumber(phone, uiDelegate: nil) { (verificationID, error) in
                    if error != nil {
                        self.statusText.text = "Error to send code"
                        print("error to send code")
                        print("Auth Error: \(String(describing: error?.localizedDescription))")
                    }else{
                        self.statusText.text = "Code sent"
                        let defaults = UserDefaults.standard
                        defaults.set(phone, forKey: "authPhoneNum")
                        defaults.set(verificationID, forKey: "authVID")
                        print("code sent")
                        self.performSegue(withIdentifier: "code", sender: Any?.self)
                    }
                }
            }
            let cancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
            
            alert.addAction(action)
            alert.addAction(cancel)
            self.present(alert, animated: true, completion: nil)
        }else{
            self.statusText.text = "Empty phone number field"
            print("empty phone number field")
        }
    }
}

